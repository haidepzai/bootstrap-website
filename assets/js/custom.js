// Start Klassenänderung
$(window).on('scroll', function() {
   if ($(window).scrollTop() > 50) { //sobald Scrollbalken an dieser Position ist
       //Navigation
       $('nav.navbar').addClass('bg-white'); //Hintergrundfarbe ändert sich in weiß, sobald wir scrollen
       //Back to Top Button
       $('#back-to-top-button').addClass('d-inline');
   } else {
       //Navigation Back
       $('nav.navbar').removeClass('bg-white');
       //Back to Top Button Back
       $('#back-to-top-button').removeClass('d-inline');
   }
});
// Ende Klassenänderung

//Start Smooth Scroll
//ready = überprüfen, ob Seite geladen ist
$(document).ready(function() {
   $('a.smooth').on('click', function(event) {
       if(this.hash !== "") {
           event.preventDefault(); //Die normale Funktion des Links verhindern
           let hash = this.hash;
           $('html, body').animate({
              scrollTop: $(hash).offset().top 
           }, 800, function(){
               window.location.hash = hash; //hash wird an der Browser location hinzugefügt.
           });
       }
   }); 
});
//Ende Smooth Scroll

//Start aktuelle Jahreszahl
let currentYear = (new Date).getFullYear();
$(document).ready(function() {
   $('#year').text(currentYear); 
});
//Ende aktuelle Jahreszahl

//Start AOS
AOS.init({
    duration: 800, //Standard duration 800ms
    easing: 'ease-out', //Animation startet ganz normal und am Ende langsamer
    offset: 300,
    disable: 'mobile'
});
//Ende AOS